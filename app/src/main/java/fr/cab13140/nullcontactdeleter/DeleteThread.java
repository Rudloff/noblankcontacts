package fr.cab13140.nullcontactdeleter;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import android.widget.TextView;

import java.util.Arrays;

class DeleteThread extends Thread {
    private ContentResolver cr;
    private TextView myText;
    private Handler theMainHandler;
    private MainActivity activity;

    DeleteThread(Handler _target, ContentResolver _cr, TextView _myText, MainActivity _activity) {
        setName("nullcontact_cleanerthread");
        this.theMainHandler = _target;
        this.cr = _cr;
        this.myText = _myText;
        this.activity = _activity;
    }

    public void run() {
        int TheProcessed;
        int TheNuked;
        int Processed = 0;
        int Nuked = 0;
        Cursor cur = this.cr.query(Contacts.CONTENT_URI, null, null, null, null);
        assert cur != null;
        while (cur.moveToNext()) {
            try {
                try {
                    final String TheName;
                    Uri uri = Uri.withAppendedPath(Contacts.CONTENT_LOOKUP_URI, cur.getString(cur.getColumnIndex("lookup")));
                    System.out.println("The uri is " + uri.toString());
                    String name = cur.getString(cur.getColumnIndex("display_name"));
                    System.out.println("The name is " + name);
                    if (name == null) {
                        this.cr.delete(uri, null, null);
                        Nuked++;
                    }
                    Processed++;
                    if (name == null) {
                        TheName = "null";
                    } else {
                        TheName = name;
                    }
                    TheProcessed = Processed;
                    TheNuked = Nuked;
                    final int finalTheProcessed = TheProcessed;
                    final int finalTheNuked = TheNuked;
                    this.theMainHandler.post(new Runnable() {
                        public void run() {
                            DeleteThread.this.myText.setText(activity.getString(R.string.processed) + Integer.toString(finalTheProcessed) + "\n"+activity.getString(R.string.deleted) + Integer.toString(finalTheNuked) + "\n"+activity.getString(R.string.current) + TheName);
                        }
                    });
                } catch (Exception e) {
                    System.out.println(Arrays.toString(e.getStackTrace()));
                }
            } catch (final Exception e2) {
                this.theMainHandler.post(new Runnable() {

                    public void run() {
                        DeleteThread.this.myText.setText(e2.getMessage());
                        activity.onThreadResult(2);
                    }
                });
                return;
            }
        }
        TheProcessed = Processed;
        TheNuked = Nuked;
        final int finalTheProcessed1 = TheProcessed;
        final int finalTheNuked1 = TheNuked;
        this.theMainHandler.post(new Runnable() {
            public void run() {
                DeleteThread.this.myText.setText(activity.getString(R.string.processed) + Integer.toString(finalTheProcessed1) + "\n"+activity.getString(R.string.deleted) + Integer.toString(finalTheNuked1));
				if (finalTheNuked1 == 0) activity.onThreadResult(3);
                else activity.onThreadResult(1);

            }
        });
    }
}
